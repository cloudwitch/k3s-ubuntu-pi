#!/bin/bash
# Download the image.
apt -y install wget
wget http://cdimage.ubuntu.com/releases/20.04/release/ubuntu-20.04-preinstalled-server-arm64+raspi.img.xz

# Write user-data into the image.
# Make it ready to be burned to a pi.
# Make sure to have a way to change the hostname when the image gets generated.
# Make the gitlab runner docker container working.
